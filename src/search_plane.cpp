#include "plane.h"
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>

pcl::PointCloud<pcl::PointXYZ> points;
int counter;

void scanCallback (const sensor_msgs::PointCloud2ConstPtr& scan);
double getDistance2 (const pcl::PointXYZ& point);


int main(int argc, char** argv)
{
    ros::init(argc, argv, "search_plane");
    ros::NodeHandle pnh("~");
    ros::Subscriber sub_scan = pnh.subscribe("/velodyne_points", 1, scanCallback);

    counter = 0;
    points.header.frame_id = "velodyne";
    points.points.resize(1);

    /* 5回分点群を取得 */
    while (counter < 5 && ros::ok)
    {
        ros::spinOnce();
        ros::Duration(0.1).sleep();
    }

    Plane plane;
    double yaw_angle;
    pcl::PointCloud<pcl::PointXYZ>::Ptr points_ptr (new pcl::PointCloud<pcl::PointXYZ>);
    *points_ptr = points;
    std::printf("%s\n", (plane.search(points_ptr, yaw_angle))? "success":"false");

    return 0;
}


void scanCallback (const sensor_msgs::PointCloud2ConstPtr& scan)
{
    pcl::PointCloud<pcl::PointXYZ> scan_pcl;
    pcl::fromROSMsg(*scan, scan_pcl);
    
    points.points.reserve(scan_pcl.points.size());
    for (auto& it: scan_pcl.points)
    {
        if (getDistance2(it) < 5.0*5.0)
        {
            points.points.push_back(it);
        }
    }
    points.points.shrink_to_fit();
    ++counter;
    std::printf("counter: %d\n", counter);
}

double getDistance2 (const pcl::PointXYZ& point)
{
    return point.x*point.x + point.y*point.y + point.z*point.z;
}
