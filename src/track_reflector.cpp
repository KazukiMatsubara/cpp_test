#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <nav_msgs/Odometry.h>
#include <tf2/LinearMath/Vector3.h>
#include <tf2/LinearMath/Quaternion.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <velodyne_pointcloud/point_types.h>
#include <visualization_msgs/MarkerArray.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>

using vecPair = std::pair<tf2::Vector3, tf2::Vector3>;
const auto FRAME_ID = "base_footprint";

void odomCallback(const nav_msgs::OdometryConstPtr& odom);
void scanCallback(const sensor_msgs::PointCloud2ConstPtr& scan);
bool detectReflector(const sensor_msgs::PointCloud2& input, const double& reflector_d, std::vector<std::pair<tf2::Vector3, tf2::Vector3>>& point_out, const double& height_base_to_velodyne, double max_range = 1.5, double mesh = 0.1);
vecPair trackReflector(const std::vector<vecPair>& reflectors, const nav_msgs::Odometry& odom_new, const nav_msgs::Odometry& odom_old);
bool registReflector(const std::vector<vecPair>& reflectors, vecPair& target_pair, const nav_msgs::Odometry& odom_new, const nav_msgs::Odometry& odom_old, const int mode, const int counter);
vecPair calculateReflectorForU(const vecPair& ref_pair, const double& ld);
geometry_msgs::Point converToGeometryPointfromTF(const tf2::Vector3& tf_v);
tf2::Vector3 rotateVector(const tf2::Vector3& p, const tf2::Quaternion& q);
tf2::Vector3 converToTFfromGeometryPoint(const geometry_msgs::Point& geo_p);
tf2::Quaternion converToTFfromGeometryQuaternion(const geometry_msgs::Quaternion& geo_q);
void initMarkers(void);

struct Grid{
    int points;
    int group;
    void init(void)
    {
        this->points = 0;
        this->group = 0;
    }
};

struct COG
{
    tf2::Vector3 position;
    int N;
    int intensity;
    double r_height;
    void init(void)
    {
        this->N = 0;
        this->intensity = 0;
    }
};


// sensor_msgs::PointCloud2 cloud;
nav_msgs::Odometry odom;
nav_msgs::Odometry odom_init;
bool get_odom;
bool get_cloud;
vecPair ref_old;
ros::Publisher pub_marker;
ros::Publisher pub_predict_marker;
visualization_msgs::MarkerArray markers;
visualization_msgs::Marker predict_marker;
tf::StampedTransform transform;
double tolerance;              // リフレクタペア探索時の許容誤差

int main(int argc, char** argv)
{
    ros::init(argc, argv, "track_reflector");

    tf::TransformListener tflistner;
    ros::Duration(2.0).sleep();
    ros::NodeHandle pnh("~");
    ros::Subscriber sub_odom = pnh.subscribe("/odom", 1, odomCallback);
    ros::Subscriber sub_scan = pnh.subscribe("/velodyne_points", 1, scanCallback);
    pub_marker = pnh.advertise<visualization_msgs::MarkerArray>("marker", 1);
    pub_predict_marker = pnh.advertise<visualization_msgs::Marker>("predict_marker", 1);

    pnh.param("tolerance", tolerance, 0.05);

    get_odom = false;
    get_cloud = false;
    ref_old = std::make_pair(tf2::Vector3(0.0, 0.0, 0.0), tf2::Vector3(0.0, 0.0, 0.0));

    initMarkers();

    while(ros::ok())
    {
        try
        {
            tflistner.lookupTransform(FRAME_ID, "velodyne", ros::Time(0), transform);
            ROS_INFO("get a transform");
            break;
        }
        catch (tf::TransformException& e)
        {
            std::cerr << e.what() << std::endl;
            return 1;
        }
    }

    // transform.frame_id_ = "velodyne";
    // transform.child_frame_id_ = "base_footprint";
    // transform.setOrigin(tf::Vector3(-0.280, 0.0, -0.284));
    // transform.setRotation(tf::Quaternion(0.0, 0.0, 0.0, 1.0));

    ros::spin();

    return 0;
}

bool trackReflector(const std::vector<vecPair>& reflectors, vecPair& target, const nav_msgs::Odometry& odom_new, const nav_msgs::Odometry& odom_old)
{
    if (ref_old.first.distance(tf2::Vector3(0.0, 0.0, 0.0)) < 0.05)
    {
        ref_old = reflectors[0];
        return false;
    }

    // 移動Vector pp_d, 回転Quaternion qq_dを計算
    auto pp_d = converToTFfromGeometryPoint(odom_old.pose.pose.position) - converToTFfromGeometryPoint(odom_new.pose.pose.position);
    auto qq_d = converToTFfromGeometryQuaternion(odom_old.pose.pose.orientation) * converToTFfromGeometryQuaternion(odom_new.pose.pose.orientation).inverse();

    // std::printf("(pp_d) x: %.3f, y: %.3f, z: %.3f\n", pp_d.getX(), pp_d.getY(), pp_d.getZ());
    // std::printf("(qq_d) x: %.3f, y: %.3f, z: %.3f, z: %.3f\n", qq_d.getX(), qq_d.getY(), qq_d.getZ(), qq_d.getW());

    // それぞれのリフレクタに対し次の操作を実施
    // bb = RR * (aa - pp)   // aa: pose_old, bb: pose_new
    // 一つ前のリフレクタ位置とオドメトリから現在のリフレクタ位置を予想
    auto ref_can = std::make_pair(rotateVector(ref_old.first - pp_d, qq_d), rotateVector(ref_old.second - pp_d, qq_d));
    for (auto& reflector: reflectors)
    {
        // 予想したリフレクタ位置と検出したリフレクタ位置を比較し、差が小さければ採用
        if (ref_can.first.distance(reflector.first) < 0.05 && ref_can.second.distance(reflector.second) < 0.05)
        {
            ref_old = reflector;
            target = reflector;
            odom_init = odom;
            return true;
        }
    }
    // 見つからなければ
    ref_old = reflectors[0];
    return false;
}

tf2::Vector3 rotateVector(const tf2::Vector3& p, const tf2::Quaternion& q)
{
    return p.rotate(q.getAxis(), q.getAngle());
}

tf2::Vector3 converToTFfromGeometryPoint(const geometry_msgs::Point& geo_p)
{
    return tf2::Vector3(geo_p.x, geo_p.y, geo_p.z);
}

tf2::Quaternion converToTFfromGeometryQuaternion(const geometry_msgs::Quaternion& geo_q)
{
    return tf2::Quaternion(geo_q.x, geo_q.y, geo_q.z, geo_q.w);
}

geometry_msgs::Point converToGeometryPointfromTF(const tf2::Vector3& tf_v)
{
    geometry_msgs::Point geo_point;
    geo_point.x = tf_v.getX();
    geo_point.y = tf_v.getY();
    geo_point.z = tf_v.getZ();
    return geo_point;
}

void odomCallback(const nav_msgs::OdometryConstPtr& odommsg)
{
    if (!get_odom) 
    {
        get_odom = true;
        odom_init = *odommsg;
    }
    // else
    //     odom_old = odom;

    odom = *odommsg;
}

void scanCallback(const sensor_msgs::PointCloud2ConstPtr& scanmsg)
{
    if (!get_cloud) 
        get_cloud = true;
    static int counter = 0;
    static bool flag_get_target = false;
    std::vector<vecPair> reflectors;

    sensor_msgs::PointCloud2 scan;
    try
    {
        pcl_ros::transformPointCloud(FRAME_ID, transform, *scanmsg, scan);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return;
    }

// 1.93
    vecPair reflector;
    if (detectReflector(scan, 1.52, reflectors, 0.3))
    {
        ROS_INFO("first chance: %d", static_cast<int>(reflectors.size()));
        // std::cout << std::boolalpha << "flag_get_target: " << flag_get_target << std::noboolalpha << std::endl;
        // 一度リフレクタを登録するまで探し続ける。見つけたら追いかける。
        if (!flag_get_target)
        {
            if (registReflector(reflectors, reflector, odom, odom_init, 0, counter))
            {
                // 5回連続で計測するまでは何もしない
                if (counter < 5)
                {
                    ++counter;
                    return;
                }
                //  初めて5回連続でリフレクタを計測できたらref_oldに登録
                if (!flag_get_target)
                {
                    flag_get_target = true;
                    ref_old = reflector;
                }
            }
            else
            {
                // 登録に失敗したらcounterをリセット
                counter = 0;
                return;
            }
        }
        else
            trackReflector(reflectors, reflector, odom, odom_init);
    }
    // // second chance にてリフレクタを見つけてもtrackはせずあくまでref_oldを登録するのみ
    // // ひとまずはセカンド・チャンスは後回し
    // else if (detectReflector(scan, 1.93, reflectors, 0.3))
    // {
    //     ROS_INFO("second chance");
    //     if (registReflector(reflectors, reflector, odom, odom_init, 1, counter))
    //     {
    //         if (counter < 5)
    //         {
    //             ++counter;
    //             return;
    //         }
    //         else
    //         {
    //             flag_get_target = true;
    //             ref_old = reflector;
    //         }
    //     }
    //     else
    //     {
    //         counter = 0;
    //         return;
    //     }
    // }
    else
    {
        return;
    }

 
    // std::printf("(left) x: %.3f, y: %.3f, z: %.3f\n", reflector.first.getX(), reflector.first.getY(), reflector.first.getZ());
    // std::printf("(right) x: %.3f, y: %.3f, z: %.3f\n", reflector.second.getX(), reflector.second.getY(), reflector.second.getZ());
    markers.markers[0].pose.position.x = reflector.first.getX();
    markers.markers[0].pose.position.y = reflector.first.getY();
    markers.markers[0].pose.position.z = reflector.first.getZ();
    markers.markers[1].pose.position.x = reflector.second.getX();
    markers.markers[1].pose.position.y = reflector.second.getY();
    markers.markers[1].pose.position.z = reflector.second.getZ();
    std::printf("publish markers\n");
    pub_marker.publish(markers);
}


void initMarkers(void)
{
    markers.markers.resize(2);
    markers.markers[0].ns = "left";
    markers.markers[1].ns = "right";
    for (auto& it: markers.markers)
    {
        it.header.frame_id = FRAME_ID;
        it.id = 0;
        it.type = visualization_msgs::Marker::SPHERE;
        it.action = visualization_msgs::Marker::ADD;
        it.scale.x = 0.3;
        it.scale.y = 0.3;
        it.scale.z = 0.3;
        it.color.a = 0.5;
        it.color.r = 1.0;
        it.color.g = 1.0;
        it.color.b = 1.0;
        it.frame_locked = false;
        it.lifetime = ros::Duration(0.1);
    }

    predict_marker.ns = "predict";
    predict_marker.header.frame_id = FRAME_ID;
    predict_marker.id = 0;
    predict_marker.type = visualization_msgs::Marker::POINTS;
    predict_marker.action = visualization_msgs::Marker::ADD;
    predict_marker.scale.x = 0.01;
    predict_marker.scale.y = tolerance;
    predict_marker.color.a = 0.5;
    predict_marker.color.r = 0.0;
    predict_marker.color.g = 0.0;
    predict_marker.color.b = 1.0;
    predict_marker.frame_locked = false;
    predict_marker.lifetime = ros::Duration(0.1);
}

const double ed_max = 0.1;                     // 踊り場奥行き方向のズレ許容値
const double ew_max = 0.1;                     // 踊り場幅方向のズレ許容値
const double et_max = 10.0 * M_PI / 180.0;     // 踊り場奥行き方向からの角度ズレ許容値
const double e_unit = 0.01;                 // 探索単位距離
const double e_angle_unit = 1.0 * M_PI / 180.0; // 探索単位角度
const double ld = 1.5;
const double lw = 2.0;
const double sw = 1.0;
const double ml = 0.497;

bool registReflector(const std::vector<vecPair>& reflectors, vecPair& target_pair, const nav_msgs::Odometry& odom_new, const nav_msgs::Odometry& odom_old, const int mode, const int counter)
{
    // referece positions
    std::vector<tf2::Vector3> refl_refs = {tf2::Vector3(-ml/2.0, lw-sw/2.0, 0.0), tf2::Vector3(ld-ml/2.0, lw-sw/2.0, 0.0), tf2::Vector3(ld-ml/2.0, -sw/2.0, 0.0)};
    vecPair refl_ref;
    if (mode == 0)
        refl_ref = std::make_pair(refl_refs[0], refl_refs[1]);
    else if (mode == 1)
        refl_ref = std::make_pair(refl_refs[1], refl_refs[2]);

    // 移動Vector pp_d, 回転Quaternion qq_dを計算
    auto pp_d = converToTFfromGeometryPoint(odom_old.pose.pose.position) - converToTFfromGeometryPoint(odom_new.pose.pose.position);
    auto qq_d = converToTFfromGeometryQuaternion(odom_new.pose.pose.orientation) * converToTFfromGeometryQuaternion(odom_old.pose.pose.orientation).inverse();

    std::vector<int> match_counter(reflectors.size());
    for (auto& it: match_counter)
        it = 0;
    // std::printf("size: %d\n", static_cast<int>(reflectors.size()));
    predict_marker.points.reserve(static_cast<int>(ed_max*2.0/e_unit * ew_max*2.0/e_unit * et_max*2.0/e_angle_unit) + 3);
    for (auto& refl: reflectors)
    {
        for (int i=0; i<=static_cast<int>(ed_max*2.0/e_unit)+1; ++i){double ed = -ed_max + e_unit*i;
            for (int j=0; j<=static_cast<int>(ew_max*2.0/e_unit)+1; ++j){double ew = -ew_max + e_unit*j;
                for (int k=0; k<=static_cast<int>(et_max*2.0/e_angle_unit); ++k){double et = -et_max + e_angle_unit*k;
                    // 誤差を含んだ姿勢座標からのリフレクタ座標ref_imを計算
                    auto ee = tf2::Vector3(ed, ew, 0.0) + pp_d;
                    auto eq = tf2::Quaternion(tf2::Vector3(0.0, 0.0, 1.0), et) * qq_d;
                    vecPair refl_im = std::make_pair(rotateVector(refl_ref.first-ee, eq.inverse()), rotateVector(refl_ref.second-ee, eq.inverse()));
                    predict_marker.points.push_back(converToGeometryPointfromTF(refl_im.first));
                    predict_marker.points.push_back(converToGeometryPointfromTF(refl_im.second));
                    // std::printf("(distance) left: %.3f, right: %.3f\n", refl_im.first.distance(refl.first), refl_im.second.distance(refl.second));
                    if (refl_im.first.distance(refl.first) < tolerance && refl_im.second.distance(refl.second) < tolerance)
                        ++match_counter[&refl - &reflectors[0]];
                }
            }
        }
    }

    pub_predict_marker.publish(predict_marker);
    predict_marker.points.clear();
    predict_marker.points.shrink_to_fit();

    // match_counterがすべて0ならfalse
    int zero_counter = 0;
    for (auto& it: match_counter)
    {
        std::printf("match_counter[%d]: %d\n", static_cast<int>(&it-&match_counter[0]), it);
        if (it == 0)
            ++zero_counter;
    }
    if (zero_counter == match_counter.size())
    {
        std::printf("no match reflector\n");
        return false;
    }

    // 最もカウントが多いindexを採用
    size_t max_index = std::distance(match_counter.begin(), std::max_element(match_counter.begin(), match_counter.end()));
    target_pair = reflectors[max_index];
    // U字かつ目の前のリフレクタを見つけたときには本来のtargetを計算によって求める
    if (mode == 1)
    {
        target_pair = calculateReflectorForU(target_pair, ld);
    }
    else if (mode == 0 && counter >= 5)
    {
        // 今後はリフレクタを更新することごにodom_initを更新
        odom_init = odom;
    }
    std::printf("**************regist reflector!*******************\n");
    return true;
}

/**
 * @brief U字かつ目の前のリフレクタを見つけたときに，本来のリフレクタペアを計算するための関数
 * 
 * @param ref_pair 
 * @param ld 
 * @return vecPair 
 */
vecPair calculateReflectorForU(const vecPair& ref_pair, const double& ld)
{
    // a1x + b1y + c1 = 0
    double a1 = -(ref_pair.second.getY() - ref_pair.first.getY());
    double b1 =  (ref_pair.second.getX() - ref_pair.first.getX());
    // double c1 = (ref_pair.second.getY() - ref_pair.first.getY())*ref_pair.first.getX() - (ref_pair.second.getX() - ref_pair.first.getX())*ref_pair.first.getY();
    // (x-x0)^2 + (y-y0)^2 = r^2  (for left)
    double x0 = ref_pair.first.getX();
    double y0 = ref_pair.first.getY();
    double r = ld;
    // D = | |
    // double D = ;
    // 
    
    vecPair target_ref_pair;
    // target_ref_pair.first
}

bool detectReflector(const sensor_msgs::PointCloud2& input, const double& reflector_d, std::vector<std::pair<tf2::Vector3, tf2::Vector3>>& point_out, const double& height_base_to_velodyne, double max_range, double mesh) 
{
    // std::cout << "Detect_reflector activated..." << std::endl;
    // Create "cloud" to keep pcl data (XYZI)
    pcl::PointCloud<pcl::PointXYZI> cloud;

    // 1.) Filtering --------------------------------------------------------------------------------- 
    // Keep pcl data in "cloud" : ROSMsg -> PCL 
    pcl::fromROSMsg(input, cloud);
    // "num" contains numbersennheiser gams of pcl (in "cloud") directly from Velodyne
    long int num = cloud.points.size();
    // "num_highInt" containsennheiser gams numbers of pcl_highInt only
    int num_highInt = 0;
    /* Filter#1: Remove points with LOW REFLECTIVITY (low intensity) */

    // std::cout << "Start filtering out low-intensity points..." << std::endl;
    // If there exits no high intensity point, end the loop
    // remove low intensity points, and too low or high points
    cloud.erase(std::remove_if(cloud.points.begin(), cloud.points.end(), [=](pcl::PointXYZI n){return (n.intensity < 60.0 || n.z < -height_base_to_velodyne || -height_base_to_velodyne+1.5 < n.z);}), cloud.end());
    // If there exits no high intensity point, end the loop
    if (cloud.points.size() == 0) 
    {
        std::cerr << "High intensity points from reflectors NOT FOUND" << std::endl;
        // ROS_ERROR("High intensity points from reflectors NOT FOUND");
        return false;
    }
        // --------------------------------------------------------------------------------------------------

    // 2.) Grouping and Labelling -----------------------------------------------------------------------
    // 1. Define the range for grouping 
    // Note: front = +x , back = -x , right = -y , left = +y
    // std::cout << "Start pcl grouping..." << std::endl;
    double x_front = -100.0, x_back = 100.0, y_left = -100.0, y_right = 100.0;
    for(int i=0; i<cloud.points.size(); i++)
    {
        // std::printf("x: %3f, y: %3f\n", cloud.points[i].x, cloud.points[i].y);
        if(cloud.points[i].x > x_front && cloud.points[i].x < max_range)    x_front = cloud.points[i].x;
        if(cloud.points[i].x < x_back && cloud.points[i].x > -max_range)    x_back  = cloud.points[i].x;
        if(cloud.points[i].y > y_left && cloud.points[i].y < max_range)     y_left  = cloud.points[i].y;
        if(cloud.points[i].y < y_right && cloud.points[i].y > -max_range)   y_right = cloud.points[i].y;
    }
    // // set a limit 
    // check whether the point is in constraint
    // remove points which is out of range
    cloud.erase(std::remove_if(cloud.begin(), cloud.end(), [&](pcl::PointXYZI n){return !(x_back<n.x && n.x<x_front && y_right<n.y && n.y<y_left);}), cloud.end());
    if (cloud.points.size() == 0)
    {
        std::cerr << "all points are out of range" << std::endl;
        return false;
    }

    // Add empty grids (5 cm == 1 channels) at borders (both sides)
    x_front += mesh*2;
    x_back  -= mesh*2;
    y_left  += mesh*2;
    y_right -= mesh*2;

    // Grid data
    int num_row = (int)((-x_back + x_front)/mesh);
    int num_column = (int)((y_left - y_right)/mesh);

    // check the grid size
    if (num_row<0 || num_column<0)
    {
        std::cerr << "size is minus value" << std::endl;
        std::printf("x_back: %f, x_front: %f\n", x_back, x_front);
        std::printf("num_row: %d, num_column: %d\n", num_row, num_column);
        return false;
    }

    Grid grid[num_row][num_column];
    // initialize grid group number
    for (auto& _row: grid) 
        for (auto& _column: _row) 
            _column.init();

    // 2. Check whether there is any point in the channel.
    int row_calculator;
    int column_calculator;
    std::vector<int> points_row(cloud.points.size());
    std::vector<int> points_column(cloud.points.size());
    std::vector<int> points_group(cloud.points.size());
    for(int i=0; i<cloud.points.size(); i++)
    {
        // Calculate grid's row/column from x,y coordinate.
        row_calculator = (int)((cloud.points[i].x-x_back)/mesh);
        column_calculator = (int)((y_left-cloud.points[i].y)/mesh);
        grid[row_calculator][column_calculator].points++;
        // Record row/column of each point
        points_row[i] = row_calculator;
        points_column[i] = column_calculator;
    }

    // 3. Group labelling (ignore border)
    // std::cout << "Start pcl labelling..." << std::endl;
    int group_index = 0;
    int surround_min = 0;
    int group_counter = 0;
    bool all_zero = false;
    std::vector<int> surround_group(8);
    std::vector<int> group_corrector(num_row*num_column);

    for(int i=0; i <(num_row)*(num_column); i++) 
        group_corrector[i] = i;
    for(int m=1; m<num_row-1; m++)
    {
        for(int n=1; n<num_column-1; n++)
        {
            if(grid[m][n].points > 0)
            {
                surround_group[0] = grid[m-1][n+1].group; surround_group[1] = grid[m][n+1].group; surround_group[2] = grid[m+1][n+1].group;
                surround_group[3] = grid[m-1][n].group;                                           surround_group[4] = grid[m+1][n].group;
                surround_group[5] = grid[m-1][n-1].group; surround_group[6] = grid[m][n-1].group; surround_group[7] = grid[m+1][n-1].group;                    
                surround_min = group_index; 

                if(*std::max_element(surround_group.begin(), surround_group.end()) == 0) all_zero = true;
                else
                {
                    all_zero = false;
                    // Find the MINIMUM from surroundings
                    for(auto it: surround_group) 
                        if(it != 0 && it < surround_min)  
                            surround_min = it;
                    // Record the correct group number in "group_corrector"
                    for(auto it: surround_group)
                    {
                        if(surround_min < it) 
                        {
                            if (it < group_corrector.size()) group_corrector[it] = surround_min;
                            else 
                            {
                                std::cerr << "group_corrector size error" << std::endl;
                                return false;
                            }
                        }  
                    }
                }
                // Label group number to the channel
                if(all_zero == true)
                {
                   group_index++;
                   grid[m][n].group = group_index;
                }
                else grid[m][n].group = surround_min;
            }
        }
    }
    // Correct group number according to "group_corrector"
    for(int m=0; m<num_row; m++) 
        for(int n=0; n<num_column; n++) 
            grid[m][n].group = group_corrector[grid[m][n].group];

    // Record group number of each point
    for(int i=0; i<cloud.points.size();i++) 
        points_group[i] = grid[points_row[i]][points_column[i]].group;

    // Count the number of groups 
    int k = 0;
    for (int m=0; m<num_row; m++)
    {
       for(int n=0; n<num_column; n++)
       {
          if(grid[m][n].group > k)
          {
              k = grid[m][n].group;
              group_counter++;
          }
       }
    }

    // Make "group_list", showing number of all existing groups.
    k = 0;
    std::vector<int> group_list(group_counter);
    for (auto& it: group_list) it = 0;
    for (int m=0; m<num_row; m++)
    {
       for(int n=0; n<num_column; n++)
       {
          if(grid[m][n].group > *std::max_element(group_list.begin(), group_list.end()))
          {
              group_list[k] = grid[m][n].group;
              k++;
          }
       }
    }
    // --------------------------------------------------------------------------------------------------

    // 3.) Group Position Calculation -------------------------------------------------------------------
    // std::cout << "Start group position calculation..." << std::endl;
    std::vector<COG> group_data(group_counter);
    double temp_x = 0.0, temp_y = 0.0;
    double temp_z_min = 100.0, temp_z_max = -100.0;
    for(auto& it: group_data) it.init();
    for(int i=0; i<group_counter; i++)
    {
        temp_x = 0.0; temp_y = 0.0;
        temp_z_min = 100.0; temp_z_max = -100.0;
        for(int j=0; j<cloud.points.size();j++)
        {
            if(points_group[j] == group_list[i])
            {
                temp_x += cloud.points[j].x;
                temp_y += cloud.points[j].y;
                group_data[i].N++; 
                group_data[i].intensity += cloud[j].intensity;
                if (cloud.points[j].z < temp_z_min) temp_z_min = cloud.points[j].z;
                if (temp_z_max < cloud.points[j].z) temp_z_max = cloud.points[j].z;
            }
        }
        if (group_data[i].N != 0)
        {
            group_data[i].position.setX(temp_x/group_data[i].N);
            group_data[i].position.setY(temp_y/group_data[i].N);
            group_data[i].r_height = std::abs(temp_z_max - temp_z_min);
        }
        else 
        {
            group_data[i].position.setX(0.0);
            group_data[i].position.setY(0.0);
        }
    }

    // ROS_INFO("(before filitering) group_data.size: %d", static_cast<int>(group_data.size()));
    /* remove groups which don't have enough intensity */
    int max_intensity = 1000;
    group_data.erase(std::remove_if(group_data.begin(), group_data.end(), [=](COG n){return n.intensity < max_intensity;}), group_data.end());
    // ROS_INFO("(after filitering) group_data.size: %d", static_cast<int>(group_data.size()));

    /* if single detect mode, return a refclector information which has heighest intensity in the group_data */
    if (reflector_d <= 0.1)
    {
        // std::cout << "single detect mode" << std::endl;
        tf2::Vector3 single = tf2::Vector3(0.0, 0.0, 0.0);
        for (auto& it: group_data)
        {
            if (max_intensity < it.intensity && 0.1 < it.r_height)
            {
                max_intensity = it.intensity;
                single = it.position;
            } 
        }
        // 一度もmax_intensityを更新できなかったらfalse
        if (max_intensity == 2000)
        {
            std::cerr << "It is not a reflector" << std::endl;
            return false;
        }
        point_out.resize(1);
        point_out[0].first = single;
        point_out[0].second = tf2::Vector3(0.0, 0.0, 0.0);
        return true;
    }

    // Consider which 2 of all groups are reflectors
    // Try to find true RIGHT reflectoro det
    // std::cout << "Consider which 2 of all groups are reflectors..." << std::endl;
    tf2::Vector3 reflector_left, reflector_right;
    bool found_check = false;
    double groups_d;
    double accepted_error = 0.1;
    double abs_dx, abs_dy;
    // reserve memory for group_counter
    point_out.reserve(group_counter);
    // std::printf("---d.size:%d\n", group_counter);
    for(int i=0; i<group_counter; i++)
    {
        for(int j=0; j<group_counter; j++)
        {
            if(i!=j)
            {
                groups_d = group_data[i].position.distance(group_data[j].position);
                // std::printf("d = %.3f\n", groups_d);
                if(reflector_d*(1.0-accepted_error) < groups_d && groups_d < reflector_d*(1.0+accepted_error))
                {
                    abs_dx = std::abs(group_data[i].position.getX() - group_data[j].position.getX());
                    abs_dy = std::abs(group_data[i].position.getY() - group_data[j].position.getY());

                    if(abs_dx >= abs_dy)
                    {
                        if(group_data[i].position.getX() < group_data[j].position.getX())
                        {
                           reflector_left = group_data[i].position;    
                           reflector_right = group_data[j].position;   
                        }
                        else if (group_data[j].position.getX() < group_data[i].position.getX())
                        {
                           reflector_left = group_data[j].position;    
                           reflector_right = group_data[i].position;   
                        }  
                        found_check = true; 
                    }
                    else if(abs_dx < abs_dy)
                    {
                        if(group_data[i].position.getY() < group_data[j].position.getY())
                        {
                           reflector_left = group_data[j].position;    
                           reflector_right = group_data[i].position;   
                        }
                        else if (group_data[j].position.getY() < group_data[i].position.getY())
                        {
                           reflector_left = group_data[i].position;    
                           reflector_right = group_data[j].position;   
                        }  
                        found_check = true; 
                    }  
                    point_out.push_back(std::make_pair(reflector_left, reflector_right));
                }
            }
        }
    }
    // std::printf("---\n");
    // release memory
    point_out.shrink_to_fit();
    // std::printf("reflectors.size = %d\n", static_cast<int>(point_out.size()));

    return found_check;
}