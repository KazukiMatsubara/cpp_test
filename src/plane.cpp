#include "plane.h"

#include <iostream>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>

void PlaneParameter::setValues(const double& a_, const double& b_, const double& c_, const double& d_)
{
    a = a_; b = b_; c = c_; d = d_;
}

void PlaneParameter::setValues(const std::vector<float>& values)
{
    setValues(values[0], values[1], values[2], values[3]);            
}

void PlaneParameter::setZero (void)
{
    setValues(0.0, 0.0, 0.0, 0.0);
}

tf2::Vector3 PlaneParameter::getNormal(void) const
{
    return tf2::Vector3(a, b, c).normalize();
}


bool Plane::search(const pcl::PointCloud<pcl::PointXYZ>::Ptr& points, double& yaw_angle)
{
    bool flag_detect_plane = false;
    PlaneParameter plane_param;
    std::vector<int> indices;
    int counter = 0;

    // 床以外の一番大きい面を見つけるまで面を除去しつつ探索
    while(!flag_detect_plane)
    {
        if(detection(points, plane_param, indices))
        {
            // 面を見つけたら法線ベクトルのピッチ方向角度を計算し、床であれば対応indicesを削除
            if (isFloor(calculatePitchAngle(plane_param)))
            {
                std::printf("This plane is floor.\n");
                for (auto& it: indices)
                {
                    points->erase(points->begin()+it);
                }
                ++counter;
                // 無限ループにならないよう、一定回数以上試行したら終了
                if (5 < counter) 
                    return false;
            }
            else
            {
                std::printf("A plane is founded.\n");
                // 床でなければ面として採用
                flag_detect_plane = true;
            }
        }
        else
        {
            // PCL面検出にて面を見つけられない場合には線の組み合わせから面を見つけたい
            
            // 見つけられなければ終了
            std::printf("A plane is NOT founded\n");
            return false;
        }
    }

    // 面と機体X軸との角度を計算
    yaw_angle = calculateYawAngle(plane_param);
    std::printf("yaw angle is %.2f\n", yaw_angle);
    return true;
}

bool Plane::detection(const pcl::PointCloud<pcl::PointXYZ>::Ptr& points, PlaneParameter& plane_param, std::vector<int>& indices)
{
    // coefficient of a plane detected
    pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
    // point number of a plane detected
    pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType (pcl::SACMODEL_PLANE);
    seg.setMethodType (pcl::SAC_RANSAC);
    seg.setDistanceThreshold (0.01);
    // filtering
    seg.setInputCloud (points);
    seg.segment (*inliers, *coefficients);
  
    if (inliers->indices.size () == 0)
    {
        std::printf("Could not estimate a planar model for the given dataset.\n");
        return false;
    }
    else if (inliers->indices.size() < 10)
    {
        std::printf("there is few points: %d\n", static_cast<int>(inliers->indices.size()));
    }
    else 
    {
      std::printf("cloud size is %d\n", static_cast<int>(points->points.size()));
      std::printf("inliers size is %d\n", static_cast<int>(inliers->indices.size()));
    }

    plane_param.setValues(coefficients->values);
    indices = inliers->indices;
    std::cout << "Model coefficients: " << coefficients->values[0] << " " 
                                        << coefficients->values[1] << " "
                                        << coefficients->values[2] << " " 
                                        << coefficients->values[3] << std::endl;
    return true;
}

double Plane::calculateYawAngle(const PlaneParameter& plane_param)
{
    /* z方向の射影ベクトルとx軸ベクトルとの内積 */
    // z方向の射影ベクトル
    tf2::Vector3 normal = plane_param.getNormal();
    normal.setZ(0.0);
    normal.normalize();
    // x軸とのなす角を計算（方向はy軸値にて判断）
    double yaw_angle = normal.getY()/std::abs(normal.getY()) * tf2::Vector3(1.0, 0.0, 0.0).angle(normal);
    if (M_PI_2 < yaw_angle <= M_PI)
        return yaw_angle - M_PI;
    else if (-M_PI <= yaw_angle < -M_PI_2)
        return yaw_angle + M_PI;
    else
        return yaw_angle;
}

double Plane::calculatePitchAngle(const PlaneParameter& plane_param)
{
    /* 法線ベクトルとZ軸のなす角 */
    return tf2::Vector3(0.0, 0.0, 1.0).angle(plane_param.getNormal());
}

bool Plane::isFloor (const double& pitch_angle)
{
    /* z軸からの角度pitch_angleが45°〜135°以内なら壁、それ以外なら床と判断 */
    return !(1.0/4.0 * M_PI <= pitch_angle && pitch_angle <= 3.0/4.0 * M_PI);
}
