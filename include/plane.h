#ifndef PLANE_H
#define PLANE_H

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <tf2/LinearMath/Vector3.h>
#include <tf2/LinearMath/Quaternion.h>


struct PlaneParameter
{
    double a;
    double b;
    double c;
    double d;
    void setValues (const double& a_, const double& b_, const double& c_, const double& d_);
    void setValues (const std::vector<float>& values);
    void setZero (void);
    tf2::Vector3 getNormal(void) const;
};

class Plane
{
private:
public:
    bool search(const pcl::PointCloud<pcl::PointXYZ>::Ptr& points, double& yaw_angle);
    bool detection(const pcl::PointCloud<pcl::PointXYZ>::Ptr& points, PlaneParameter& plane_param, std::vector<int>& indices);
    double calculateYawAngle(const PlaneParameter& plane_param);
    double calculatePitchAngle(const PlaneParameter& plane_param);
    bool isFloor (const double& pitch_angle);
};

#endif // PLANE_H